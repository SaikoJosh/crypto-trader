import * as algo from '../src/trader/algorithm';
import { getCurrencyPairPool } from '../src/trader/operations';
import * as config from '../src/config';
import { log } from '../src/logger';
import { db } from '../src/database';

const {
	MAX_WEIGHT_PER_SIDE_PCT,
	MIN_TRADE_PCT,
	MAX_TRADE_PCT,
	MIN_TRADE_VALUE,
	MAX_TRADE_VALUE,
	GRADUATION_SIZE_PPT,
	PIN_CURRENCY_ID,
} = config.constants;

const [, arg1, arg2] = process.argv;

void (async function main(): Promise<void> {
	try {
		log.info(`Script: Test Trade`);

		const windowStart = new Date(arg1!);
		const windowEnd = new Date(arg2!);
		log.info(`Window start: ${windowStart}`);
		log.info(`Window end: ${windowEnd}`);

		const pair = await db.currencyPair.findFirst({
			where: { status: `TRADABLE`, baseCurrencyId: `ATOM`, quoteCurrencyId: `BNB` },
			include: {
				BaseCurrency: true,
				QuoteCurrency: true,
				Trade: { where: { dateFulfilled: { not: null } }, orderBy: { dateFulfilled: `desc` }, take: 5 },
			},
		});
		log.info(`Found pair "${pair!.baseCurrencyId}/${pair!.quoteCurrencyId}"`);

		const pool = await getCurrencyPairPool(pair!, windowStart, windowEnd);
		const rulesV1: algo.v1.TradeRules = {
			maxWeight: MAX_WEIGHT_PER_SIDE_PCT,
			minTradablePct: MIN_TRADE_PCT,
			maxTradablePct: MAX_TRADE_PCT,
			minTradableValue: MIN_TRADE_VALUE,
			maxTradableValue: MAX_TRADE_VALUE,
			graduationSize: GRADUATION_SIZE_PPT,
		};
		const [action, tradeCurrencyId, againstCurrencyId, value] = await algo.v1.determineTradeAction(pool, rulesV1);

		log.info(`Algo V1 result: ${action} ${tradeCurrencyId} using ${PIN_CURRENCY_ID} ${value} of ${againstCurrencyId}`);
	} catch (e: unknown) {
		const err = e as NodeJS.ErrnoException;
		log.error(err.message);
	}
})();
