import { Spot } from '@binance/connector';
import * as config from '../src/config';
import { log } from '../src/logger';

const client = new Spot(config.env.BINANCE_API_KEY, config.env.BINANCE_API_SECRET);

void (async function main(): Promise<void> {
	try {
		const result = await (client as any).myTrades(`ALGOUSDT` /*, { fromId: `96985539` }*/);

		log.info(`Trades:`, result.data);
	} catch (e: unknown) {
		const err = e as NodeJS.ErrnoException;
		log.error(err.message);
	}
})();
