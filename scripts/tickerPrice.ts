import { Spot } from '@binance/connector';
import * as config from '../src/config';
import { log } from '../src/logger';
import type { AxiosError } from '../src/lib/definitions';

const client = new Spot(config.env.BINANCE_API_KEY, config.env.BINANCE_API_SECRET);

void (async function main(): Promise<void> {
	try {
		const result = await (client as any).tickerPrice();

		const filtered = result.data.filter((item: any) => item.symbol.includes(`ATOM`));
		log.info(`Result:`, filtered);
	} catch (e: unknown) {
		const err = e as AxiosError;
		const { code, msg } = err.response.data;
		log.error(err.message);
		log.error(`${code}: ${msg}`);
	}
})();
