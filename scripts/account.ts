import { Spot } from '@binance/connector';
import * as config from '../src/config';
import { log } from '../src/logger';

const client = new Spot(config.env.BINANCE_API_KEY, config.env.BINANCE_API_SECRET);
const currencies = [`BTC`, `ETH`, `USDT`, `ALGO`];

void (async function main(): Promise<void> {
	try {
		const result = await (client as any).account();
		const balances = result.data.balances
			.filter((balance: any) => currencies.includes(balance.asset))
			.map((balance: any) => `${balance.asset}: ${balance.free}`);

		delete result.data.balances;

		log.info(`Account:`, result.data);
		log.info(`Balances:`, balances);
	} catch (e: unknown) {
		const err = e as NodeJS.ErrnoException;
		log.error(err.message);
	}
})();
