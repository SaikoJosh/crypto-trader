import { db } from '../src/database';

void (async function main(): Promise<void> {
	// Currencies: Fiat.
	await db.currency.create({ data: { id: `EUR`, type: `FIAT`, minTradableAmount: `10` } });
	await db.currency.create({ data: { id: `GBP`, type: `FIAT`, minTradableAmount: `10` } });
	await db.currency.create({ data: { id: `USD`, type: `FIAT`, minTradableAmount: `10` } });
	// Currencies: Stablecoin.
	await db.currency.create({ data: { id: `USDT`, type: `STABLECOIN`, minTradableAmount: `10` } });
	// Currencies: Crypto.
	await db.currency.create({ data: { id: `ALGO`, type: `CRYPTO`, minTradableAmount: `10` } });
	await db.currency.create({ data: { id: `ATOM`, type: `CRYPTO`, minTradableAmount: `0.1` } });
	await db.currency.create({ data: { id: `BNB`, type: `CRYPTO`, minTradableAmount: `0.01` } });
	await db.currency.create({ data: { id: `BTC`, type: `CRYPTO`, minTradableAmount: `0.0001` } });
	await db.currency.create({ data: { id: `ETH`, type: `CRYPTO`, minTradableAmount: `0.001` } });

	// Currency Pairs.
	await db.currencyPair.create({ data: { status: `TRADABLE`, baseCurrencyId: `ATOM`, quoteCurrencyId: `BNB` } });
})();
