# Define global args.
ARG APP_DIR="/opt/crypto-trader"
ARG NODE_VERSION="17.4.0"
ARG ALPINE_VERSION="3.15"

# Build.
FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION} AS build-image
ARG APP_DIR
ENV NODE_ENV=development
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
COPY ./src ${APP_DIR}/src
COPY ./prisma ${APP_DIR}/prisma
COPY ./package.json ${APP_DIR}/package.json
COPY ./package-lock.json ${APP_DIR}/package-lock.json
COPY ./tsconfig.json ${APP_DIR}/tsconfig.json
RUN npm install --no-progress --no-audit --no-update-notifier --no-fund --verbose
RUN npm run build

# Productionise.
FROM build-image AS final-image
ARG APP_DIR
ENV NODE_ENV=production
RUN apk --no-cache add curl
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
COPY --from=build-image ${APP_DIR}/dist ${APP_DIR}/dist
COPY --from=build-image ${APP_DIR}/package.json ${APP_DIR}/package.json
COPY --from=build-image ${APP_DIR}/package-lock.json ${APP_DIR}/package-lock.json
RUN npm install --no-progress --no-audit --no-update-notifier --no-fund --verbose
CMD ["npm", "start", "--silent"]
