const IGNORE_PATHS = [`<rootDir>/node_modules`, `<rootDir>/dist`];

module.exports = {
	collectCoverageFrom: [
		`**/*.{js,jsx,ts,tsx}`,
		`!**/.eslintrc.js`,
		`!**/.prettierrc.js`,
		`!**/node_modules/**`,
		`!**/coverage/**`,
		`!**/dist/**`,
		`!**/test/**`,
	],
	coverageReporters: [`text`, `text-summary`],
	coverageThreshold: {
		global: {
			branches: 100,
			functions: 100,
			lines: 100,
			statements: 100,
		},
	},
	errorOnDeprecated: true,
	testPathIgnorePatterns: [...IGNORE_PATHS],
	coveragePathIgnorePatterns: [...IGNORE_PATHS],
	testTimeout: 5_000,
	verbose: false,
	testMatch: ['**/*.spec.[jt]s?(x)'],
	testEnvironment: `node`,
	preset: 'ts-jest',
};
