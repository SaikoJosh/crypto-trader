/**
 * Currencies.
 */
export const PIN_CURRENCY_ID = `USDT`;

/**
 * Trade Rules.
 */
export const BASE_TRADABLE_AMOUNT_PCT = 0.5;
export const MAX_WEIGHT_PER_SIDE_PCT = 0.8;
export const MIN_TRADE_PCT = 0.01;
export const MAX_TRADE_PCT = 0.5;
export const MIN_TRADE_VALUE = 10;
export const MAX_TRADE_VALUE = 100;
export const GRADUATION_SIZE_PPT = 0.5;

/**
 * Durations.
 */
export const SECONDS = 1000 * 1;
export const MINUTES = 1000 * 60 * 1;
export const HOURS = 1000 * 60 * 60 * 1;
export const DAYS = 1000 * 60 * 60 * 24 * 1;

/**
 * Timings.
 */
export const INTERVAL_OBTAIN_PRICES_MS = 10 * SECONDS;
export const INTERVAL_CALC_AVG_PRICES_MS = 30 * SECONDS;
export const INTERVAL_DETERMINE_ORDERS_MS = 30 * MINUTES;
export const TRADE_DELAY_MS = 0.1 * SECONDS;
