import dotenv from 'dotenv';

dotenv.config();

export const NODE_ENV = process.env.NODE_ENV || `development`; // eslint-disable-line @typescript-eslint/prefer-nullish-coalescing -- handle empty strings.
// export const KRAKEN_API_KEY = process.env.KRAKEN_API_KEY as string;
// export const KRAKEN_API_PRIVATE_KEY = process.env.KRAKEN_API_PRIVATE_KEY as string;
export const BINANCE_API_KEY = process.env.BINANCE_API_KEY as string;
export const BINANCE_API_SECRET = process.env.BINANCE_API_SECRET as string;
