const CACHE = new Map<string, any>();

export function get(key: string): any {
	return CACHE.get(key);
}

export async function load(): Promise<void> {
	//
}

export async function init(): Promise<void> {
	CACHE.set(`LOG_LEVEL`, `trace`);
}
