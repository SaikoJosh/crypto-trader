declare module '@binance/connector' {
	/**
	 * Miscellaneous types.
	 */
	export type DecimalString = string;

	export type AccountType = `SPOT`;

	export type Permission = `SPOT` | `MARGIN` | `LEVERAGED` | `TRD_GRP_002`;

	export type OrderStatus =
		| `NEW`
		| `PARTIALLY_FILLED`
		| `FILLED`
		| `CANCELED`
		| `PENDING_CANCEL`
		| `REJECTED`
		| `EXPIRE`;

	export type OrderType =
		| `LIMIT`
		| `MARKET`
		| `STOP_LOSS`
		| `STOP_LOSS_LIMIT`
		| `TAKE_PROFIT`
		| `TAKE_PROFIT_LIMIT`
		| `LIMIT_MAKER`;

	export type OrderSide = `BUY` | `SELL`;

	export type TimeInForce = `GTC` | `IOC` | `FOK`;

	export interface Response<ResponseData> {
		data: ResponseData;
	}

	/**
	 * Spot Account.
	 */
	export interface SpotAccountCurrencyBalance<Asset> {
		asset: Asset;
		free: DecimalString;
		locked: DecimalString;
	}

	export interface SpotAccount<Asset> {
		makerCommission: number;
		takerCommission: number;
		buyerCommission: number;
		sellerCommission: number;
		canTrade: boolean;
		canWithdraw: boolean;
		canDeposit: boolean;
		updateTime: number;
		accountType: AccountType;
		permissions: Permission[];
		balances: SpotAccountCurrencyBalance<Asset>[];
	}

	export type SpotAccountResponse<Asset> = Response<SpotAccount<Asset>>;

	/**
	 * Spot Ticker.
	 */
	export interface SpotTickerPriceEntry {
		symbol: string;
		price: string;
	}

	export type SpotTickerPriceSingleResponse = Response<SpotTickerPriceEntry>;
	export type SpotTickerPriceMultipleResponse = Response<SpotTickerPriceEntry[]>;
	export type SpotTickerPriceResponse = SpotTickerPriceSingleResponse | SpotTickerPriceMultipleResponse;

	/**
	 * Spot Klines.
	 */
	export interface SpotKlinesOptions {
		startTime: number;
		endTime: number;
		limit?: number;
	}

	export type KlineOpenTime = number;
	export type KlineOpen = string;
	export type KlineHigh = string;
	export type KlineLow = string;
	export type KlineClose = string;
	export type KlineVolume = string;
	export type KlineCloseTime = number;
	export type KlineQuoteAssetVolume = string;
	export type KlineNumTrades = number;
	export type KlineTakerBuyBaseAssetValue = string;
	export type KlineTakerBuyQuoteAssetVolume = string;
	export type KlineIgnore = string;

	export type SpotKlinesTuple = [
		KlineOpenTime,
		KlineOpen,
		KlineHigh,
		KlineLow,
		KlineClose,
		KlineVolume,
		KlineCloseTime,
		KlineQuoteAssetVolume,
		KlineNumTrades,
		KlineTakerBuyBaseAssetValue,
		KlineTakerBuyQuoteAssetVolume,
		KlineIgnore,
	];

	export type SpotKlinesResponse = Response<SpotKlinesTuple[]>;

	/**
	 * Spot New Order.
	 */
	export interface SpotNewOrderOptions {
		timeInForce?: string;
		quantity?: number;
		quoteOrderQty?: number;
		price?: DecimalString;
		newClientOrderId?: string;
		stopPrice?: DecimalString;
		icebergQty?: number;
		newOrderRespType?: string;
		recvWindow?: number;
	}

	export interface SpotNewOrderAck {
		symbol: string;
		orderId: number;
		orderListId: number;
		clientOrderId: string;
		transactTime: number;
	}

	export interface SpotNewOrderResult {
		symbol: string;
		orderId: number;
		orderListId: number;
		clientOrderId: string;
		transactTime: number;
		price: DecimalString;
		origQty: DecimalString;
		executedQty: DecimalString;
		cumulativeQuoteQty: DecimalString;
		status: string;
		timeInForce: string;
		type: string;
		side: string;
	}

	export type SpotNewOrderAckResponse = Response<SpotNewOrderAck>;
	export type SpotNewOrderResultResponse = Response<SpotNewOrderResult>;
	export type SpotNewOrderResponse = SpotNewOrderAckResponse | SpotNewOrderResultResponse;

	/**
	 * Spot Class.
	 */
	export class Spot {
		public constructor(apiKey: string, apiSecret: string);

		public account<RT = SpotAccountResponse<string>>(): Promise<RT>;

		public klines<RT = SpotKlinesResponse>(symbol: string, interval: string, options?: SpotKlinesOptions): Promise<RT>;

		public tickerPrice<RT = SpotTickerPriceResponse>(symbol?: string): Promise<RT>;

		public newOrder<RT = SpotNewOrderResponse>(
			symbol: string,
			side: string,
			type: OrderType,
			options?: SpotNewOrderOptions,
		): Promise<RT>;
	}
}
