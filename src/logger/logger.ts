import events from 'events';
import util from 'util';
import * as dateFns from 'date-fns';
import type * as stream from 'stream';
import type { Prisma } from '@prisma/client';

/**
 * Dictionary of logable facets.
 */
export interface IFacets {
	[key: string]: IFacets | string | number | boolean | Date | Prisma.Decimal | undefined;
}

/**
 * Dictionary of details for a single log line.
 */
export interface ILogDetails {
	level: string;
	message: string;
	facets: IFacets | undefined;
	dateLogged: Date;
}

const INSPECT_OPTS: util.InspectOptions = {
	colors: true,
	depth: 100,
};

export default class Logger extends events.EventEmitter {
	/**
	 * Writes the given log arguments to the given stream.
	 * @param stream - The stream to write to.
	 * @param level - The log level.
	 * @param message - The message to log out.
	 * @param facets - Facets to log out, optional.
	 */
	// eslint-disable-next-line @typescript-eslint/no-explicit-any -- log output can be anything.
	private writeLog(stream: stream.Writable, level: string, message: string, facets: IFacets | undefined): void {
		const facetsStr = facets ? util.inspect(facets, INSPECT_OPTS) : undefined;
		const dateLogged = new Date();
		const dateStr = dateFns.formatISO9075(dateLogged);
		const line = `[${level}][${dateStr}] ${message}${facetsStr ? `\n${facetsStr}` : ``}\n`;

		stream.write(line);
		this.emit(`write`, { level, message, facets, dateLogged });
	}

	/**
	 * Logs out at error level.
	 * @param message - The message to log out.
	 * @param facets - Facets to log out, optional.
	 */
	public error(message: string, facets?: IFacets): void {
		this.writeLog(process.stderr, `ERROR`, message, facets);
	}

	/**
	 * Logs out at info level.
	 * @param message - The message to log out.
	 * @param facets - Facets to log out, optional.
	 */
	public info(message: string, facets?: IFacets): void {
		this.writeLog(process.stdout, `INFO`, message, facets);
	}

	/**
	 * Logs out at trace level.
	 * @param message - The message to log out.
	 * @param facets - Facets to log out, optional.
	 */
	public trace(message: string, facets?: IFacets): void {
		this.writeLog(process.stdout, `TRACE`, message, facets);
	}
}
