import { Prisma } from '@prisma/client';
import { log } from '../../logger';
import type { dbTypes } from '../../database';
import type { CurrencyPairPool, TradeAction } from '../definitions';

/**
 * Dictionary of trade rules for the algo.
 */
export interface TradeRules {
	maxWeight: number; // e.g. 80% of pool total value.
	minTradablePct: number; // e.g. 1%.
	maxTradablePct: number; // e.g. 15%.
	minTradableValue: number; // In pin currency, e.g. 10 GBP.
	maxTradableValue: number; // In pin currency, e.g. 1000 GBP.
	graduationSize: number; // e.g. a 0.147 pct price change / 0.05 gradation size = 2 full graduations (rounded down).
}

/**
 * Dictionary counting the number of each trade action performed recently.
 */
type RecentTradeActions = {
	[key in TradeAction]: number;
};

/**
 * Dictionary of allowable trade actions.
 */
type TradeActionsAllowed = {
	[key in TradeAction]: boolean;
};

/**
 * Dictionary counting the votes for each trade action.
 */
type TradeActionVotes = {
	[key in TradeAction]: number;
};

/**
 * Dictionary containing information on the result of the vote.
 */
interface VoteResult {
	is: {
		BUY: boolean;
		SELL: boolean;
		HOLD: boolean;
	};
	diff: number;
	tradeAction: TradeAction;
}

export interface TradeDetails {
	action: TradeAction;
	tradeCurrencyId: dbTypes.CurrencySymbol;
	againstCurrencyId: dbTypes.CurrencySymbol;
	value: Prisma.Decimal;
	baseWeightRemaining: number;
	quoteWeightRemaining: number;
	numGradations: number;
}

/**
 * Returns the amount of 'weight' remaining in the pool for the given currency side, determined by the given rules.
 * @param pool - The currency pair pool information.
 * @param rules - A dictionary of trading rules to apply.
 * @param side - Whether to calculate for the base or quote currency.
 */
function calcWeightRemainingOnPoolSide(pool: CurrencyPairPool, rules: TradeRules, side: `base` | `quote`): number {
	return rules.maxWeight - pool[side].weight;
}

/**
 * Counts the number of price graduations that have occurred based on the graduation size.
 * @param rules - A dictionary of trading rules to apply.
 * @param changePct - The percentage change in price that has occurred.
 */
function countGradations(rules: TradeRules, changePct: number): number {
	return Math.floor(changePct / rules.graduationSize) - 1;
}

/**
 * Counts the actions performed on the given list of recent trades and returns a tuple of a  dictionary of trade action
 * counts and the last trade action performed.
 * @param trades - List of recent trades performed.
 */
function countRecentTrades(trades: dbTypes.Trade[]): [RecentTradeActions, TradeAction] {
	const recentTrades: RecentTradeActions = { BUY: 0, SELL: 0, HOLD: 0 };
	const lastTrade = trades[trades.length - 1]?.side ?? `HOLD`;

	for (const trade of trades) {
		switch (trade.side) {
			case `BUY`:
				recentTrades.BUY++;
				break;

			case `SELL`:
				recentTrades.SELL++;
				break;

			default:
				recentTrades.HOLD++;
				break;
		}
	}

	return [recentTrades, lastTrade];
}

/**
 * Disables buy/sell actions that are not allowed because either side of the pool is too heavy.
 * @param allowedActions - A mutable dictionary of trade actions that are allowed.
 * @param baseWeightRemaining - The amount of 'weight' remaining in the pool for the base currency.
 * @param quoteWeightRemaining - The amount of 'weight' remaining in the pool for the quote currency.
 */
function applyRuleMaxWeight(
	allowedActions: TradeActionsAllowed,
	baseWeightRemaining: number,
	quoteWeightRemaining: number,
): void {
	if (baseWeightRemaining <= 0) allowedActions.BUY = false;
	if (quoteWeightRemaining <= 0) allowedActions.SELL = false;

	log.info(`Max weight rule applied`, allowedActions);
}

/**
 * Collates votes based on the price direction.
 * @param pool - The currency pair pool information.
 * @param votes - A mutable dictionary of votes that have been collated.
 */
function voteOnPriceDirection(pool: CurrencyPairPool, votes: TradeActionVotes): void {
	const { direction } = pool;

	if (direction === `UP`) votes.SELL += 1;
	if (direction === `DOWN`) votes.BUY += 1;
	if (direction === `FLAT`) votes.HOLD += 1;

	log.info(`Voted on price direction`, votes);
}

/**
 * Collates votes based on the number of graduations in the price change.
 * @param pool - The currency pair pool information.
 * @param votes - A mutable dictionary of votes that have been collated.
 * @param numGradations - The number of price change graduations that have occurred.
 */
function voteOnPriceGraduations(pool: CurrencyPairPool, votes: TradeActionVotes, numGradations: number): void {
	const { direction } = pool;

	if (direction === `UP`) votes.SELL += Math.max(0, numGradations);
	if (direction === `DOWN`) votes.BUY += Math.max(0, numGradations);

	log.info(`Voted on price graduations`, votes);
}

/**
 * Collates votes based on the recent trades that have been placed.
 * @param votes - A mutable dictionary of votes that have been collated.
 * @param recentTrades - A dictionary of recent trades performed.
 * @param lastTrade - The last trade action performed.
 */
function voteOnRecentTrades(votes: TradeActionVotes, recentTrades: RecentTradeActions, lastTrade: TradeAction): void {
	votes.SELL += recentTrades.BUY;
	votes.BUY += recentTrades.SELL;
	if (recentTrades.HOLD > 0) votes.HOLD -= 1;

	if (lastTrade === `BUY`) votes.SELL += 1;
	if (lastTrade === `SELL`) votes.BUY += 1;

	log.info(`Voted on recent trades`, votes);
}

/**
 * Counts the votes for each trade action and returns the winning trade action.
 * @param votes - A dictionary of trade actions and their corresponding vote counts.
 * @param allowedActions - A dictionary of allowed trade actions.
 */
function countVotes(votes: TradeActionVotes, allowedActions: TradeActionsAllowed): VoteResult {
	const diff = Math.abs(votes.BUY - votes.SELL);
	let topVoteQty: number = 0;
	let topVoteAction: TradeAction = `HOLD`;

	for (const [voteAction, voteQty] of Object.entries(votes)) {
		if (!allowedActions[voteAction as TradeAction]) continue;
		if (voteQty < 1) continue;
		if (voteQty < topVoteQty) continue;

		topVoteAction = voteQty === topVoteQty ? `HOLD` : (voteAction as TradeAction); // Always hold if tied.
		topVoteQty = voteQty;
	}

	const result: VoteResult = {
		is: {
			BUY: topVoteAction === `BUY`,
			SELL: topVoteAction === `SELL`,
			HOLD: topVoteAction === `HOLD`,
		},
		diff,
		tradeAction: topVoteAction,
	};

	return result;
}

/**
 * Calculates the maximum trade value that is allowed based on the remaining weight in the pool.
 * @param pool - The currency pair pool information.
 * @param baseWeightRemaining - The amount of 'weight' remaining in the pool for the base currency.
 * @param quoteWeightRemaining - The amount of 'weight' remaining in the pool for the quote currency.
 * @param voteResult - The result of the vote.
 */
function calcMaxTradeValueByWeight(
	pool: CurrencyPairPool,
	baseWeightRemaining: number,
	quoteWeightRemaining: number,
	voteResult: VoteResult,
): Prisma.Decimal {
	const { totalValue } = pool;

	const weightRemaining = voteResult.is.BUY ? baseWeightRemaining : quoteWeightRemaining;
	const result = new Prisma.Decimal(weightRemaining).times(totalValue);

	return result;
}

/**
 * Calculates the ideal percentage trade that we want to make based on the given rules, price change graduations, and
 * the result of the vote.
 * @param rules - A dictionary of trading rules to apply.
 * @param numGradations - The number of price change graduations that have occurred.
 * @param voteResult - The result of the vote.
 */
function calcDesiredTradePercent(rules: TradeRules, numGradations: number, voteResult: VoteResult): number {
	const { minTradablePct: floor, maxTradablePct: ceiling, graduationSize } = rules;

	const target =
		floor + // Minimum percentage of value we aim to trade.
		numGradations * graduationSize + // Increase trade size if there is a big price movement.
		voteResult.diff * graduationSize; // Increase trade size if there is a strong vote in one direction.
	const result = target < floor ? floor : target > ceiling ? ceiling : target;

	return result;
}

/**
 * Calculates the actual amount of value we want to trade based on the given rules, the desired trade percentage, and
 * the maximum tradable value allowed.
 * @param pool - The currency pair pool information.
 * @param rules - A dictionary of trading rules to apply.
 * @param desiredTradePct - The percentage of the total value that we want to trade.
 * @param maxTradeValue - The maximum trade value that is allowed.
 */
function calcActualTradeValue(
	pool: CurrencyPairPool,
	rules: TradeRules,
	desiredTradePct: number,
	maxTradeValue: Prisma.Decimal,
): Prisma.Decimal {
	const { base } = pool;

	const floor = new Prisma.Decimal(rules.minTradableValue);
	const ceiling = Prisma.Decimal.min(rules.maxTradableValue, maxTradeValue);
	const target = base.pinnedValue.times(desiredTradePct);
	const result = target.lt(floor) ? floor : target.gt(ceiling) ? ceiling : target;

	return result;
}

/**
 * Takes in data on the currency pair pool and a set of rules and returns the next trade action to perform and the value
 * of the trade to make.
 * @param pool - The currency pair pool information.
 * @param rules - A dictionary of trading rules to apply.
 */
export function determineTradeAction(pool: CurrencyPairPool, rules: TradeRules): TradeDetails {
	const { totalValue, direction, changePct, trades, base, quote } = pool;
	const allowedActions: TradeActionsAllowed = { BUY: true, SELL: true, HOLD: true };
	const votes: TradeActionVotes = { BUY: 0, SELL: 0, HOLD: 0 };
	const baseWeightRemaining = calcWeightRemainingOnPoolSide(pool, rules, `base`);
	const quoteWeightRemaining = calcWeightRemainingOnPoolSide(pool, rules, `quote`);
	const numGradations = countGradations(rules, changePct);
	const [recentTrades, lastTrade] = countRecentTrades(trades);

	log.info(`Pool info:`, { totalValue, base: { ...base }, quote: { ...quote } });
	log.info(`Weight remaining:`, { baseWeightRemaining, quoteWeightRemaining });
	log.info(`Price change:`, { direction, changePct, numGradations });
	log.info(`Recent trades:`, { recentTrades, lastTrade });

	// Hard rules.
	applyRuleMaxWeight(allowedActions, baseWeightRemaining, quoteWeightRemaining);

	// Voting.
	voteOnPriceDirection(pool, votes);
	voteOnPriceGraduations(pool, votes, numGradations);
	voteOnRecentTrades(votes, recentTrades, lastTrade);
	const voteResult = countVotes(votes, allowedActions);

	log.info(`Vote result:`, { votes, ...voteResult });

	if (voteResult.is.HOLD) {
		return {
			action: voteResult.tradeAction,
			tradeCurrencyId: `xxxxxxx`,
			againstCurrencyId: `xxxxxxx`,
			value: new Prisma.Decimal(0),
			baseWeightRemaining,
			quoteWeightRemaining,
			numGradations,
		};
	}

	// Calculation.
	const desiredTradePct = calcDesiredTradePercent(rules, numGradations, voteResult);
	const maxTradeValue = calcMaxTradeValueByWeight(pool, baseWeightRemaining, quoteWeightRemaining, voteResult);
	const tradeValue = calcActualTradeValue(pool, rules, desiredTradePct, maxTradeValue);
	const tradeCurrencyId = voteResult.is.BUY ? base.id : quote.id;
	const againstCurrencyId = voteResult.is.BUY ? quote.id : base.id;

	return [voteResult.tradeAction, tradeCurrencyId, againstCurrencyId, tradeValue];
}
