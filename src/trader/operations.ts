import { Prisma } from '@prisma/client';
import * as exchange from '../exchange';
import * as config from '../config';
import type { dbTypes } from '../database';
import type { PriceDirection, CurrencyPairRecord, CurrencyPairPool } from './definitions';

const { PIN_CURRENCY_ID } = config.constants;

/**
 * Returns the first and last price point from the given list of price points.
 * @param pricePoints - list of price points.
 */
function getBoundingPricePoints(pricePoints: Prisma.Decimal[]): [Prisma.Decimal, Prisma.Decimal] {
	const firstPricePoint = pricePoints[0];
	const lastPricePoint = pricePoints[pricePoints.length - 1];

	if (pricePoints.length < 2 || !firstPricePoint || !lastPricePoint) {
		throw new Error(`Need at least 2 price points to determine price direction, we have ${pricePoints.length}`);
	}

	return [firstPricePoint, lastPricePoint];
}

/**
 * Returns price change data (price direction and percentage change) about the given base currency within the given time
 * window.
 * @param baseCurrency - The currency to use as the base.
 * @param quoteCurrency - The currency to use for quoting.
 * @param windowStart - The start of the time window used for data points.
 * @param windowEnd - The end of the time window for data points.
 */
async function getCurrencyChangeData(
	baseCurrency: dbTypes.CurrencySymbol,
	quoteCurrency: dbTypes.CurrencySymbol,
	windowStart: Date,
	windowEnd: Date,
): Promise<[PriceDirection, number]> {
	const pricePoints = await exchange.binance.getHistoricalPricePoints(
		baseCurrency,
		quoteCurrency,
		windowStart,
		windowEnd,
	);
	const [first, last] = getBoundingPricePoints(pricePoints);
	const diff = last.sub(first);
	const direction = diff.lt(0) ? `DOWN` : diff.gt(0) ? `UP` : `FLAT`;
	const changePct = Math.abs(diff.div(first).times(100).toNumber());

	return [direction, changePct];
}

/**
 * Returns information about the currency pair pool including the currency balances, pinned values, and weights.
 * @param pair - the currency pair to get the pool information for.
 * @param windowStart - The start of the time window used for data points.
 * @param windowEnd - The end of the time window for data points.
 */
export async function getCurrencyPairPool(
	pair: CurrencyPairRecord,
	windowStart: Date,
	windowEnd: Date,
): Promise<CurrencyPairPool> {
	const { baseCurrencyId, quoteCurrencyId, Trade: trades } = pair;
	const balances = await exchange.binance.getAccountBalances([baseCurrencyId, quoteCurrencyId]);
	const [direction, changePct] = await getCurrencyChangeData(baseCurrencyId, quoteCurrencyId, windowStart, windowEnd);
	const baseBalance = balances[baseCurrencyId] ?? new Prisma.Decimal(0);
	const quoteBalance = balances[quoteCurrencyId] ?? new Prisma.Decimal(0);
	const basePinnedValue = await exchange.binance.getPinnedValue(baseCurrencyId, baseBalance, PIN_CURRENCY_ID);
	const quotePinnedValue = await exchange.binance.getPinnedValue(quoteCurrencyId, quoteBalance, PIN_CURRENCY_ID);
	const totalValue = basePinnedValue.plus(quotePinnedValue);
	const baseWeight = basePinnedValue.div(totalValue).toNumber();
	const quoteWeight = quotePinnedValue.div(totalValue).toNumber();

	const pool = {
		totalValue,
		direction,
		changePct,
		trades,
		base: {
			id: baseCurrencyId,
			balance: baseBalance,
			pinnedValue: basePinnedValue,
			weight: baseWeight,
		},
		quote: {
			id: quoteCurrencyId,
			balance: quoteBalance,
			pinnedValue: quotePinnedValue,
			weight: quoteWeight,
		},
	};

	return pool;
}
