import { db } from '../database';
import type { CurrencyPairRecord } from './definitions';

/**
 * Returns a list of currency records for the crypto currencies we can trade.
 */
export async function getTradableCurrencyPairs(): Promise<CurrencyPairRecord[]> {
	const result = await db.currencyPair.findMany({
		where: { status: `TRADABLE` },
		include: {
			BaseCurrency: true,
			QuoteCurrency: true,
			Trade: { where: { dateFulfilled: { not: null } }, orderBy: { dateFulfilled: `desc` }, take: 5 },
		},
	});
	return result;
}

/**
 * Inserts a new trade into the database.
 */
export async function insertTrade(): Promise<void> {
	//
}
