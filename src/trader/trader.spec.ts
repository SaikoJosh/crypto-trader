import { getTradeAction } from './trader';

describe(`#getTradeAction`, () => {
	describe(`pair weights balanced 50:50`, () => {
		describe(`small currency price decrease`, () => {
			test(`buy flip from sell`, () => {
				const currencyDirection = `DOWN`;
				const currencyChangePct = 0.05;
				const currencyWeight = 0.5;
				const sisterWeight = 0.5;
				const lastTradeSide = `SELL`;

				const [tradeAction, amountPct] = getTradeAction(
					currencyDirection,
					currencyChangePct,
					currencyWeight,
					sisterWeight,
					lastTradeSide,
				);

				expect(tradeAction).toBe(`BUY`);
				expect(amountPct).toBe(0.05);
			});
		});

		test(`buy repeat`, () => {
			//
		});
	});

	describe(`small currency price increase`, () => {
		test(`sell flip from buy`, () => {
			const currencyDirection = `UP`;
			const currencyChangePct = 0.05;
			const currencyWeight = 0.5;
			const sisterWeight = 0.5;
			const lastTradeSide = `BUY`;

			const [tradeAction, amountPct] = getTradeAction(
				currencyDirection,
				currencyChangePct,
				currencyWeight,
				sisterWeight,
				lastTradeSide,
			);

			expect(tradeAction).toBe(`SELL`);
			expect(amountPct).toBe(0.05);
		});

		test(`sell repeat`, () => {
			//
		});
	});
});
