import * as algo from './algorithm';
import { getTradableCurrencyPairs } from './queries';
import { getCurrencyPairPool } from './operations';
import { log } from '../logger';
import * as config from '../config';
import * as exchange from '../exchange';
import { sleep } from '../lib/utilities';
import type { CurrencyPairRecord } from './definitions';
import { db } from '../database';

const {
	MAX_WEIGHT_PER_SIDE_PCT,
	MIN_TRADE_PCT,
	MAX_TRADE_PCT,
	MIN_TRADE_VALUE,
	MAX_TRADE_VALUE,
	GRADUATION_SIZE_PPT,
	TRADE_DELAY_MS,
	INTERVAL_DETERMINE_ORDERS_MS,
	PIN_CURRENCY_ID,
} = config.constants;

/**
 * Trades the given currency pair based on price data from within the given time window.
 * @param pair - the currency pair to get the pool information for.
 * @param windowStart - The start of the time window used for data points.
 * @param windowEnd - The end of the time window for data points.
 */
async function tradeCurrencyPair(pair: CurrencyPairRecord, windowStart: Date, windowEnd: Date): Promise<void> {
	const pool = await getCurrencyPairPool(pair, windowStart, windowEnd);
	const rulesV1: algo.v1.TradeRules = {
		maxWeight: MAX_WEIGHT_PER_SIDE_PCT,
		minTradablePct: MIN_TRADE_PCT,
		maxTradablePct: MAX_TRADE_PCT,
		minTradableValue: MIN_TRADE_VALUE,
		maxTradableValue: MAX_TRADE_VALUE,
		graduationSize: GRADUATION_SIZE_PPT,
	};
	const tradeDetails = algo.v1.determineTradeAction(pool, rulesV1);

	log.info(`Algo V1 result: ${action} ${PIN_CURRENCY_ID} ${value} of ${tradeCurrencyId}/${againstCurrencyId}`);

	await db.trade.create({
		data: {
			currencyPairId: `${tradeCurrencyId}/${againstCurrencyId}`,
			action,
			baseWeightBefore
			baseAmountBefore
			quoteWeightBefore
			quoteAmountBefore
			tradeValue: value,
		},
	});

	if (action === `HOLD`) return;

	await exchange.binance.placeOrder(action, `MARKET`, tradeCurrencyId!, againstCurrencyId!, value!.toNumber());
}

/**
 * Trades all tradeable currency pairs one after another.
 */
async function tradeCurrencyPairs(): Promise<void> {
	const windowStart: Date = new Date(); // TODO: Get this from the database.
	const windowEnd: Date = new Date(); // Now.
	const pairs = await getTradableCurrencyPairs();

	for (const pair of pairs) {
		await tradeCurrencyPair(pair, windowStart, windowEnd);
		await sleep(TRADE_DELAY_MS);
	}
}

/**
 * Initialise the module.
 */
export async function init(): Promise<void> {
	log.info(`Initialising trader`);

	setInterval(tradeCurrencyPairs, INTERVAL_DETERMINE_ORDERS_MS);
	await Promise.resolve(); // Placeholder.

	log.info(`Trader initialised`);
}
