import type { dbTypes } from '../database';
import type { Prisma } from '@prisma/client';

/**
 * The currency pair record including the base and quote currency relations.
 */
export type CurrencyPairRecord = dbTypes.CurrencyPair & {
	BaseCurrency: dbTypes.Currency;
	QuoteCurrency: dbTypes.Currency;
	Trade: dbTypes.Trade[];
};

/**
 * The currency record including the trade relation.
 */
export type CurrencyRecord = dbTypes.Currency & {
	Trade: dbTypes.Trade[] | undefined;
};

/**
 * The current pair pool item describing a single currency within the pool.
 */
export interface CurrencyPairPoolItem {
	id: dbTypes.CurrencySymbol;
	balance: Prisma.Decimal;
	pinnedValue: Prisma.Decimal;
	weight: number;
}

/**
 * The current pair pool including the base and quote currency balances, pinned values, and weights.
 */
export interface CurrencyPairPool {
	totalValue: Prisma.Decimal;
	direction: PriceDirection;
	changePct: number;
	trades: dbTypes.Trade[];
	base: CurrencyPairPoolItem;
	quote: CurrencyPairPoolItem;
}

/**
 * Represents possible price directions.
 */
export type PriceDirection = `UP` | `DOWN` | `FLAT`;

/**
 * Represents possible trading actions.
 */
// export type TradeAction = `BUY` | `BUY_AGAIN` | `SELL` | `SELL_AGAIN` | `HOLD`;
export type TradeAction = `BUY` | `SELL` | `HOLD`;
