export interface AxiosError extends Error {
	response: {
		data: any; // eslint-disable-line @typescript-eslint/no-explicit-any -- data can be anything.
		status: number;
		headers: {
			[key: string]: string;
		};
	};
}
