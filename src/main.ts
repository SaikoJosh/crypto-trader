import { log } from './logger';
import { init as initDb } from './database';
import { init as initExchange } from './exchange';
// import { init as initMonitor } from './monitor';
import { init as initTrader } from './trader';

/**
 * Main entry point for the app.
 */
void (async function main(): Promise<void> {
	log.info(`Starting up...`);

	const db = await initDb();
	await initExchange();
	// await initMonitor();
	await initTrader();

	log.on(
		`write`,
		({ level, message, facets, dateLogged }): void =>
			void db.logEntry.create({ data: { level, message, facets, dateLogged } }).catch(() => {
				// NOTE: Do nothing in this case and allow logs to be lost.
			}),
	);

	log.info(`Ready!`);
})();
