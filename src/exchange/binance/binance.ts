import { Spot } from '@binance/connector';
import { Prisma } from '@prisma/client';
import { log } from '../../logger';
import * as config from '../../config';
import type {
	OrderSide,
	OrderType,
	SpotKlinesOptions,
	SpotKlinesResponse,
	SpotKlinesTuple,
	SpotNewOrderOptions,
	SpotAccountResponse,
	SpotTickerPriceSingleResponse,
} from '@binance/connector';
import type { AxiosError } from '../../lib/definitions';
import type { dbTypes } from '../../database';

/**
 * Represents a dictionary of currencies and their balances.
 * */
export type AccountBalances = {
	[key in dbTypes.CurrencySymbol]?: Prisma.Decimal;
};

/**
 * The various modes that can be used to calculate historical price points.
 */
export type HistoryMode = `FULL_AVERAGE`;

/**
 * The Binance API client.
 */
export const client = new Spot(config.env.BINANCE_API_KEY, config.env.BINANCE_API_SECRET);

/**
 * Obtains balances for the given currencies and returns them as a dictionary.
 * @param currencies - List of currencies to obtain balances  for.
 */
export async function getAccountBalances(currencies: dbTypes.CurrencySymbol[]): Promise<AccountBalances> {
	log.info(`Obtaining account balances for ${currencies.length} currencies...`);

	const output: AccountBalances = {};
	let res;

	try {
		res = await client.account<SpotAccountResponse<dbTypes.CurrencySymbol>>();
	} catch (e: unknown) {
		const err = e as AxiosError;
		const { code, msg } = err.response.data;
		throw new Error(`Failed to get account balances because: ${code} - ${msg}`);
	}

	for (const balance of res.data.balances) {
		if (!currencies.includes(balance.asset)) continue;
		output[balance.asset] = new Prisma.Decimal(balance.free);
	}

	log.info(`Successfully obtained account balances`, output);

	return output;
}

/**
 * Returns the value of the given currency in the given pinned currency.
 * @param currency - The currency to get the pinned value for.
 * @param balance - The current balance of the currency.
 * @param pinCurrency - The currency to pin the value to.
 */
export async function getPinnedValue(
	currency: dbTypes.CurrencySymbol,
	balance: Prisma.Decimal,
	pinCurrency: dbTypes.CurrencySymbol,
): Promise<Prisma.Decimal> {
	const value = await getTickerPrice(currency, pinCurrency);
	return value.times(balance);
}

/**
 * Returns the ticker price for the given left currency in the given right currency.
 * @param baseCurrency - The currency to get the price for.
 * @param quoteCurrency - The currency to convert to.
 */
export async function getTickerPrice(
	baseCurrency: dbTypes.CurrencySymbol,
	quoteCurrency: dbTypes.CurrencySymbol,
): Promise<Prisma.Decimal> {
	log.info(`Obtaining ticker price for currency "${baseCurrency}" in "${quoteCurrency}"...`);

	const symbol = `${baseCurrency}${quoteCurrency}`;
	let res;

	try {
		res = await client.tickerPrice<SpotTickerPriceSingleResponse>(symbol);
	} catch (e: unknown) {
		const err = e as AxiosError;
		const { code, msg } = err.response.data;
		throw new Error(
			`Failed to get ticker price for currency "${baseCurrency}" in "${quoteCurrency}" because: ${code} - ${msg}`,
		);
	}

	const price = new Prisma.Decimal(res.data.price);
	log.info(`Ticker price for currency "${baseCurrency}" in "${quoteCurrency}" is: ${price} ${quoteCurrency}`);

	return price;
}

/**
 * Returns an array of kline tuples for the given symbol and time window.
 * @param baseCurrency - The currency to get the price for.
 * @param quoteCurrency - The currency to convert to.
 * @param windowStart - The start of the window to get the price for.
 * @param windowEnd - The end of the window to get the price for.
 * @param interval - The interval between price points,  e.g. "1m", "1h", "1d", etc.
 */
export async function getKlines(
	baseCurrency: dbTypes.CurrencySymbol,
	quoteCurrency: dbTypes.CurrencySymbol,
	windowStart: Date,
	windowEnd: Date,
	interval: string,
): Promise<SpotKlinesTuple[]> {
	log.info(`Obtaining klines for currency "${baseCurrency}" in "${quoteCurrency}"...`);

	const symbol = `${baseCurrency}${quoteCurrency}`;
	let res;

	try {
		const options: SpotKlinesOptions = { startTime: windowStart.valueOf(), endTime: windowEnd.valueOf() };
		res = await client.klines<SpotKlinesResponse>(symbol, interval, options);
	} catch (e: unknown) {
		const err = e as AxiosError;
		const { code, msg } = err.response.data;
		throw new Error(`Failed to get klines for currency "${baseCurrency}" because: ${code} - ${msg}`);
	}

	log.info(`Found ${res.data.length} klines for currency "${baseCurrency}" in "${quoteCurrency}"`);
	return res.data;
}

/**
 * Takes in a calculation mode and kline tuple, and returns a price point.
 * @param mode - The mode to use to calculate the price point.
 * @param kline - The kline tuple to calculate the price point from.
 */
export function convertKlineToPricePoint(mode: HistoryMode, kline: SpotKlinesTuple): Prisma.Decimal {
	const [, kOpen, kHigh, kLow, kClose] = kline;
	const open = new Prisma.Decimal(kOpen);
	const high = new Prisma.Decimal(kHigh);
	const low = new Prisma.Decimal(kLow);
	const close = new Prisma.Decimal(kClose);

	switch (mode) {
		case `FULL_AVERAGE`:
			return open.plus(high).plus(low).plus(close).dividedBy(4);

		default:
			throw new Error(`Unsupported history mode "${mode}"`);
	}
}

/**
 * Returns an array of price points for the given currency and time window.
 * @param baseCurrency - The currency to get the price for.
 * @param quoteCurrency - The currency to convert to.
 * @param windowStart - The start of the window to get the price for.
 * @param windowEnd - The end of the window to get the price for.
 * @param interval - The interval between price points,  e.g. "1m", "1h", "1d", etc.
 * @param mode - The mode to use to calculate the price point.
 */
export async function getHistoricalPricePoints(
	baseCurrency: dbTypes.CurrencySymbol,
	quoteCurrency: dbTypes.CurrencySymbol,
	windowStart: Date,
	windowEnd: Date,
	interval: string = `1h`,
	mode: HistoryMode = `FULL_AVERAGE`,
): Promise<Prisma.Decimal[]> {
	log.info(`Obtaining historical price points for currency "${baseCurrency}" in "${quoteCurrency}"...`);
	const klines = await getKlines(baseCurrency, quoteCurrency, windowStart, windowEnd, interval);
	const pricePoints = klines.map(kline => convertKlineToPricePoint(mode, kline));

	log.info(`Found ${klines.length} klines and ${pricePoints.length} price points`);
	return pricePoints;
}

/**
 * Places a new order on the exchange with the given details.
 * @param side - The side of the order for the trade currency.
 * @param type - The type of order to place.
 * @param tradeCurrency - The currency to buy or sell.
 * @param againstCurrency - The currency to exchange with.
 * @param quantity - The quantity of the trade currency to buy or sell.
 */
export async function placeOrder(
	side: OrderSide,
	type: OrderType,
	tradeCurrency: dbTypes.CurrencySymbol,
	againstCurrency: dbTypes.CurrencySymbol,
	quantity: number,
): Promise<void> {
	log.info(`Placing "${side}" "${type}" order for "${tradeCurrency}" in "${againstCurrency}"...`);

	const symbol = `${tradeCurrency}${againstCurrency}`;
	let res;

	try {
		const options: SpotNewOrderOptions = {
			price: `10`,
			quantity,
			timeInForce: `GTC`,
		};
		res = await client.newOrder(symbol, side, type, options);
	} catch (e: unknown) {
		const err = e as AxiosError;
		const { code, msg } = err.response.data;
		throw new Error(`Failed to get ticker price for currency "${tradeCurrency}" because: ${code} - ${msg}`);
	}

	const orderId = res.data.orderId;
	log.info(`Placed "${side}" "${type}" order "${orderId}" for "${tradeCurrency}" in "${againstCurrency}"`);
}

/**
 * Initialises the module.
 */
export async function init(): Promise<void> {
	log.info(`Initialising Binance exchange`);
	await Promise.resolve(); // Do nothing.
	log.info(`Binance exchange initialised`);
}
