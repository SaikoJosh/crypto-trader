import * as binance from './binance/binance';
import { log } from '../logger';

export { binance };

/**
 * Initialises the module.
 */
export async function init(): Promise<void> {
	log.info(`Initialising exchange`);
	await binance.init();
	log.info(`Exchanged initialised`);
}
