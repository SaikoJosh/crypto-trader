// import * as dateFns from 'date-fns';
// import { db } from '../database';
import { log } from '../logger';
// import * as exchange from '../exchange';
// import * as config from '../config';

// async function obtainCurrencyPrices(): Promise<void> {
// 	log.info(`Obtaining crypto currency prices...`);

// 	const currencies = await db.currency.findMany({ select: { id: true }, where: { type: `CRYPTO` } });
// 	log.info(`Found ${currencies.length} crypto currencies`);

// 	for (const currency of currencies) {
// 		const { id: symbol } = currency;
// 		const price = await exchange.binance.getTickerPrice(symbol);
// 		await db.pricePoint.create({ data: { price: price.toString(), currencyId: symbol } });
// 	}

// 	log.info(`Successfully obtained crypto currency prices`);
// }

// function isFirstMinuteOfHour(date: Date): boolean {
// 	const minOfHour = dateFns.getMinutes(date);
// 	return minOfHour === 0;
// }

// function getHoursToFill(lastDateTimeHour: Date, curDateTimeHour: Date): Date[] {
// 	let tick: Date = lastDateTimeHour;
// 	const hoursToFill: Date[] = [];

// 	while (tick <= curDateTimeHour) {
// 		tick = dateFns.addHours(tick, 1);
// 		hoursToFill.push(tick);
// 	}

// 	return hoursToFill;
// }

// async function calculateHourlyAveragePrices(): Promise<void> {
// 	const date = new Date();
// 	if (!isFirstMinuteOfHour(date)) return;

// 	const curDateTimeHour = dateFns.startOfHour(date);
// 	const currencies = await db.currency.findMany({ select: { id: true }, where: { type: `CRYPTO` } });

// 	for (const currency of currencies) {
// 		const lastAvgHourlyPrice = await db.avgPriceHour.findFirst({
// 			select: { dateTimeHour: true },
// 			where: { currencyId: currency.id },
// 			orderBy: { dateTimeHour: `desc` },
// 		});

// 		if (!lastAvgHourlyPrice) continue;

// 		const hoursToFill = getHoursToFill(lastAvgHourlyPrice.dateTimeHour, curDateTimeHour);

// 		for (const dateHour of hoursToFill) {
// 			const avgPrice = await db.pricePoint.aggregate({
// 				// eslint-disable-next-line @typescript-eslint/naming-convention -- Prisma aggregate function.
// 				_avg: {
// 					price: true,
// 				},
// 				where: { currencyId: currency.id, dateTimeLogged: { gte: dateHour } },
// 			});

// 			if (avgPrice._avg.price === null) continue;

// 			await db.avgPriceHour.create({
// 				data: { price: avgPrice._avg.price, currencyId: currency.id, dateTimeHour: dateHour },
// 			});
// 		}
// 	}
// }

/**
 * Initialise the module.
 */
export async function init(): Promise<void> {
	log.info(`Initialising monitor`);

	// await obtainCurrencyPrices();
	// setInterval(obtainCurrencyPrices, config.constants.INTERVAL_OBTAIN_PRICES_MS);
	// setInterval(calculateHourlyAveragePrices, config.constants.INTERVAL_CALC_AVG_PRICES_MS);

	log.info(`Monitor initialised`);
}
