import { PrismaClient } from '@prisma/client';
import { log } from '../logger';
import type * as dbTypes from '@prisma/client';

export const db = new PrismaClient();
export type { dbTypes };

/**
 * Initialise the module.
 */
export async function init(): Promise<PrismaClient> {
	log.info(`Connecting to DB`);
	await db.$connect();
	log.info(`DB connected`);

	return db;
}
